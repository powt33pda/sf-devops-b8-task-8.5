# Install tools
sudo apt-get update
sudo apt install ca-certificates

# Install Jenkins
curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo tee /usr/share/keyrings/jenkins-keyring.asc > /dev/null
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] https://pkg.jenkins.io/debian-stable binary/ | sudo tee /etc/apt/sources.list.d/jenkins.list > /dev/null
sudo apt-get update
sudo apt-get install -y jenkins

# Install Java
sudo apt install -y openjdk-11-jdk

# Install MySQL Client
sudo apt-get install -y mysql-client

# Install Nginx
sudo apt-get install -y nginx
chmod a+w /var/www/html/index.nginx-debian.html